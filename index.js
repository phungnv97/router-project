var express = require('express')
    wine = require('./routes/wines');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));

app.get('/wines', wine.findAll);
app.get('/wines/:winesid', wine.findById);
app.post('/wines', wine.addWine);
app.put('/wines/:winesid', wine.updateWine);
app.delete('/wines/:winesid', wine.deleteWine);

app.listen(3000);
console.log('Listening on port 3000...');