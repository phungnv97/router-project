var mongo = require('mongodb');
var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure,
    mongoClient = mongo.MongoClient;
var ObjectID = mongo.ObjectID;
var url = 'mongodb://192.168.1.144:27017/phung_db';    

//Connect to database named 'phung_db'
mongoClient.connect(url, function(err, db) {
    if(!err) {
        console.log("Connected to 'phung_db' database");
        db.collection('wines', {strict:true}, function(err, collection) {
            if (err) {
                console.log("The 'wines' collection doesn't exist. Creating it with sample data...");
                populateDB();
            }
        });
    }
});

exports.findAll = function(req, res) {
    mongoClient.connect(url, function (err, db) {
        var wines = db.collection('wines');
        wines.find({}).toArray(function (err, results) {
            if(err) throw err;
            data = '<table border="1" style="border-collapse:collapse" cellspacing="5" cellpadding="15">';
            data += '<tr><th>Name</th><th>Year</th><th>Grapes</th><th>Country</th><th>Region</th><th>Desciption</th><th>Picture</th></tr>';
            results.forEach(function (row) {
                data += '<tr>';
                data += '<td>' + row.name + '</td>';
                data += '<td>' + row.year + '</td>';
                data += '<td>' + row.grapes + '</td>';
                data += '<td>' + row.country + '</td>';
                data += '<td>' + row.region + '</td>';
                data += '<td>' + row.description + '</td>';
                data += '<td>' + row.picture + '</td>';
                data += '</tr>';
            });
            data += '</table>';
            res.writeHead(200, {'Content-Type': 'text/html'});
            console.log('show completed !');
            res.end(data);
        });
    });
};

exports.addWine = function(req, res) {
    mongoClient.connect(url, function (err, db) {
        var wines = db.collection('wines');
        var wine = req.body;
        console.log('Adding wine: ' + JSON.stringify(wine));
        wines.insert(wine, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred'});
            } else {
                console.log('Success: ' , result.ops);
                res.send(result);
            }
        });
    });
};

exports.findById = function(req, res) {
    mongoClient.connect(url, function (err, db) {
        var id = req.params.winesid;
        var wines = db.collection('wines');
        console.log('Retrieving wine: ' + id);
        wines.findOne({'_id': ObjectID(id)}, function(err, item) {
            console.log(item);
            res.send(item);
        });
    });
};

exports.updateWine = function(req, res) {
    mongoClient.connect(url, function (err, db) {
        var id = req.params.winesid;
        var wines = db.collection('wines');
        var wine = req.body;
        console.log('Updating wine: ' + id);
        console.log(JSON.stringify(wine));
        wines.update({'_id':ObjectID(id)}, wine, function(err, result) {
            if (err) {
                console.log('Error updating wine: ' + err);
                res.send({'error':'An error has occurred'});
            } else {
                console.log('' + result + ' document(s) updated');
                res.send(wine);
            }
        });
        
    });
}

exports.deleteWine = function(req, res) {
    mongoClient.connect(url, function (err, db) {
        var id = req.params.winesid;
        var wines = db.collection('wines');
        console.log('Deleting wine: ' + id);
        wines.remove({'_id':ObjectID(id)}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred - ' + err});
            } else {
                console.log('' + result + ' document(s) deleted');
                res.send(req.body);
            }
        });
    });
}

/*--------------------------------------------------------------------------------------------------------------------*/
// Populate database with sample data 
// Only used once: the first time the application is started.

var populateDB = function() {

    var wines = [
    {
        name: "CHATEAU DE SAINT COSME",
        year: "2009",
        grapes: "Grenache / Syrah",
        country: "France",
        region: "Southern Rhone",
        description: "The aromas of fruit and spice...",
        picture: "saint_cosme.jpg"
    },
    {
        name: "LAN RIOJA CRIANZA",
        year: "2006",
        grapes: "Tempranillo",
        country: "Spain",
        region: "Rioja",
        description: "A resurgence of interest in boutique vineyards...",
        picture: "lan_rioja.jpg"
    }];

    db.collection('wines', function(err, collection) {
        collection.insert(wines, {safe:true}, function(err, result) {});
    });

};
